SNAPS
Frédéric Galusik
%%mtime(%m/%d/%Y)

%!target: man
%!encoding: utf-8
%!outfile: snaps.1


= NOM =[nom]

SNAPS - An OpenBSD admin utility to upgrade to last -current base snapshot.


= SYNOPSIS =[synopsis]

**snaps** [//options//]


= DESCRIPTION =[description]

**snaps** will help you to download the needed **bsd.rd**, check it and move it to
the right place before rebooting. A backup of your last bsd.rd is made. All
steps are verbose. It can be run with only the base system. Nevertheless if you
install //curl// from packages, it will check your //$MIRROR// build date of
base and packages. You will be able to decide if you want to do the update right now
or wait for the synchronicity of the two directories.


= GENERAL OPTIONS =[general-options]

: **-h**
Print help and exit. It also print the content of the
//$MIRROR// variable based upon ///etc/pkg.conf// for a quick look.

: **-g**
Fetch and check (SHA256 and signify) bsd.rd from //$MIRROR// (no need
to be root for that one).

: **-G**
Fetch, check bsd.rd from //$MIRROR// then, move it to the right place,
backup your last bsd.rd, print some reminders and reboot.

: **-p**
Update your //-current// port tree.

: **-l**
List date and download places of the 3 last //-G// from
/var/log/snaps.log.

: **-u**
Upgrade snaps to the last release.


= COMMAND LINE USAGE =[command-line-usage]

Here are some usage examples:

: **MIRROR=http://another/openbsd/mirror doas snaps -G**
Bypass your ///etc/pkg.conf// file and manually setup the //$MIRROR//
variable. Note that you just have to indicate the root of the //$MIRROR//.
This one have to follow the OpenBSD directory layout. Moreover, in order to play
with //MIRROR//, **doas** have to be setup with the //keepenv// option.


= WEBSITE =[website]

https://framagit.org/fredg/snaps


= AUTHOR =[author]

Frédéric Galusik


= SEE ALSO =[see-also]

**sysmerge**(8), **pkg_add**(1)
